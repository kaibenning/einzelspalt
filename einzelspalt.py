from __future__ import division
import numpy as np
from uncertainties import ufloat
from uncertainties import unumpy

data_mat = np.loadtxt('Werte.txt', skiprows=1)

x = data_mat[:,0]
y = data_mat[:,1]
x_err = data_mat[:,2]
y_err = data_mat[:,3]
x_for_chi = x
y_for_chi = y

from matplotlib import pyplot as plt

plt.errorbar(x, y, xerr=x_err, yerr=y_err,color = 'k', marker = 'x', ls='none',label=r"$\mathrm{Messpunkte}$")

def einzelspalt(x,*params):
    beta = params[0]
    off = params[1]
    x = x + off
    zaehler = np.sin(beta*x)
    nenner = (beta*x)
    return (zaehler / nenner)**2
def redchisqg(ydata,ymod,sd,deg=2):
    chisq=np.sum( ((ydata-ymod)/sd)**2 )
    nu=ydata.size-1-deg
    return np.around(chisq/nu, decimals = 2)

from scipy.optimize import curve_fit

#guess
guess = [0.25,0]
#fit
popt, pcov = curve_fit(einzelspalt, x, y, guess)
x = np.linspace(min(x),max(x),100)
#red_chi
chi_red = redchisqg(y_for_chi,einzelspalt(x_for_chi,*popt),y_err,deg=1)
plt.plot(x, einzelspalt(x, *popt), 'g-',label = r"$\mathrm{Fit: \chi ^{2} _{red} = %3.2f}$"%chi_red)
#guess
plt.plot(x,einzelspalt(x,*guess), 'b--', label=r"$\mathrm{initial \, guess}$")
#horizontale Linie bei Null
plt.axhline(0,linewidth=1, color='r',linestyle = '--')
#vertikale Linie bei Null
plt.axvline(0,linewidth=1, color='r',linestyle = '--')

plt.legend(loc = 'upper left')
plt.xlabel(r"$\mathrm{x \, [mm]}$")
plt.ylabel(r"$\mathrm{U / U_0 [\,]}$")
plt.xlim([-45,45])
plt.ylim([-0.1,1.3])
plt.grid()

print "guess: ", guess
print "Fit: ", popt
print "Fehler: ", np.sqrt(np.diag(pcov))

plt.show()
